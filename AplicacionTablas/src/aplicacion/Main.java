package aplicacion;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import aplicacion.excepciones.NotFoundException;
import aplicacion.interfaz.AbstractView;
import aplicacion.interfaz.ConsultarCiudades;
import aplicacion.interfaz.Informacion;
import aplicacion.interfaz.Inicio;
import aplicacion.interfaz.Registrar;
import aplicacion.modelo.City;
import aplicacion.service.CityService;
import aplicacion.service.CityServiceImpl;

public class Main {

	private static CityService cityService;
	private JFrame frame;
	private AbstractView inicio, ConsultarCiudades, registrar;
	private Informacion info;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Main() {
		cityService = new CityServiceImpl();
		inicio = new Inicio();
		ConsultarCiudades = new ConsultarCiudades(this);
		registrar = new Registrar(this);
		info = new Informacion();
		initialize();
	}

	public void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("src/aplicacion/interfaz/imagen/icono_app.png"));
		frame.setTitle("Menú");
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addComponents();
	}

	private void addComponents() {
		JMenuBar menuBar = new JMenuBar();
		JMenu mnCiudades = new JMenu("Ciudades");
		menuBar.add(mnCiudades);
		JMenuItem mntmInsertar = new JMenuItem("Alta");
		mntmInsertar.setActionCommand("registrar");
		mntmInsertar.addActionListener(al);
		mnCiudades.add(mntmInsertar);

		JMenuItem mntmConsultar = new JMenuItem("Buscar");
		mntmConsultar.setActionCommand("consultar");
		mntmConsultar.addActionListener(al);
		mnCiudades.add(mntmConsultar);

		JMenu mnAplicacion = new JMenu("Aplicación");
		menuBar.add(mnAplicacion);

		JMenuItem mntmInfo = new JMenuItem("Acerca de...");
		mntmInfo.setActionCommand("info");
		mntmInfo.addActionListener(al);
		mnAplicacion.add(mntmInfo);

		JMenuItem mntmExit = new JMenuItem("Salir");
		mntmExit.setAccelerator(
				KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK | InputEvent.ALT_DOWN_MASK));
		mntmExit.setActionCommand("salir");
		mntmExit.addActionListener(al);
		mnAplicacion.add(mntmExit);
		frame.setJMenuBar(menuBar);

		cambiarView(inicio);
	}

	public void cambiarView(AbstractView view) {
		frame.setContentPane(view);
		view.initialize();
		frame.revalidate();
	}

	public List<City> getCiudades(String filtro) {
		try {
			try {
				List<City> ciudad = new ArrayList<>();
				ciudad.add(cityService.getCity(Long.parseLong(filtro)));
				return ciudad;
			} catch (NumberFormatException e) {
			}
			return cityService.getCities(filtro);
		} catch (NotFoundException e) {
			lanzarPopUp(e.getMessage());
			return null;
		}
	}
	
public void lanzarPopUp(String mensaje) {		
	JOptionPane.showMessageDialog(frame, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public City registrarCiudad(City city) {
		return cityService.createCity(city);
	}

	public void salir() {
		System.exit(0);
	}

	public void acercaDe() {
		info.setVisible(true);

	}

	ActionListener al = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand() == "consultar") {
				cambiarView(ConsultarCiudades);
			}
			if (e.getActionCommand() == "registrar") {
				cambiarView(registrar);
			}
			if (e.getActionCommand() == "salir") {
				Integer i = JOptionPane.showConfirmDialog(null, "¿esta seguro de que quieres salir?", "Alerta",
						JOptionPane.WARNING_MESSAGE);
				if (i == 0) {
					salir();
				}
			}
			if (e.getActionCommand() == "info") {
				acercaDe();
			}
		}
	};

}
