package aplicacion.modelo;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.table.AbstractTableModel;



public class TableModel extends AbstractTableModel {

	private static final long serialVersionUID = 8773839253832613588L;
	private List<String> nombres;
	private List<City> datos;

	public TableModel(List<City> datos, List<String> nombres) {
		this.datos = datos;
		this.nombres = nombres;
	}

	public void cambiarNombre(List<String> nombres) {
		this.nombres = nombres;
	}
	
	@Override
	public String getColumnName(int column) {
		return nombres.get(column);
	}

	@Override
	public int getRowCount() {
		return datos.size();
	}

	@Override
	public int getColumnCount() {
		return nombres.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return recogerValor(columnIndex, rowIndex);
	}
	public Object recogerValor(int i, int j) {
		String columna = getColumnName(i);
		if (columna.equals("id")) {
			return datos.get(j).getId();
		}else if (columna.equals("descripcion")) {
			return datos.get(j).getDescripcion();
		}else if (columna.equals("countryId")) {
			return datos.get(j).getCountryId();
		}
		return null;
	}
	public void setDatos(List<City> datos) {
		this.datos = datos;
	}
	
	public void ordenar(String nombre, List<String> nombres) {
		if (nombre.equals(nombres.get(0))) {
			Collections.sort(datos, new Comparator<City>() {
				@Override
				public int compare(City o1, City o2) {
					return o1.getId().compareTo(o2.getId());
				}
			});
		} else if (nombre.equals(nombres.get(1))) {
			Collections.sort(datos, new Comparator<City>() {
				@Override
				public int compare(City o1, City o2) {
					return o1.getDescripcion().compareToIgnoreCase(o2.getDescripcion());
				}
			});
		} else if (nombre.equals(nombres.get(2))) {
			Collections.sort(datos, new Comparator<City>() {
				@Override
				public int compare(City o1, City o2) {
					return o1.getCountryId().compareTo(o2.getCountryId());
				}
			});
		}
	}
	

}
