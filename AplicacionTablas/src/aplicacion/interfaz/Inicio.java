package aplicacion.interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Inicio extends AbstractView {

	private static final long serialVersionUID = -3505393633396697618L;

	public Inicio() {
		setLayout(null);
		setLayout(null);
		JLabel lblImagen = new JLabel();
		lblImagen.setBounds(20, 25, 640, 369);
		add(lblImagen);
		lblImagen.setIcon(new ImageIcon("src/aplicacion/interfaz/imagen/fondo_inicio.png"));

	}

	@Override
	public void initialize() {
	}

}
