package aplicacion.interfaz;

import javax.swing.JTextField;

import aplicacion.Main;
import aplicacion.modelo.City;

import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;

public class Registrar extends AbstractView {
	private static final long serialVersionUID = -7869916950181018852L;
	private JTextField tfId;
	private JTextField tfDescripcion;
	private JTextField tfCountryId;
	private JButton btnRegistrar;
	private Main main;

	public Registrar(Main main) {
		this.main = main;
		setLayout(null);

		tfDescripcion = new JTextField();
		tfDescripcion.setBounds(350, 153, 86, 20);
		tfDescripcion.setColumns(10);
		tfDescripcion.addKeyListener(kl);
		add(tfDescripcion);

		tfCountryId = new JTextField();
		tfCountryId.setBounds(350, 204, 86, 20);
		tfCountryId.setColumns(10);
		tfCountryId.addKeyListener(kl);
		add(tfCountryId);

		JLabel lblDescripcion = new JLabel("Descripción");
		lblDescripcion.setBounds(218, 156, 70, 14);
		add(lblDescripcion);

		JLabel lblCountryId = new JLabel("CountryId");
		lblCountryId.setBounds(218, 207, 70, 14);
		add(lblCountryId);

		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(249, 323, 136, 42);
		btnRegistrar.addActionListener(al);
		add(btnRegistrar);
		
		tfId = new JTextField();
		tfId.setBounds(350, 105, 86, 20);
		tfId.setColumns(10);
		tfId.setEditable(false);
		add(tfId);
		
		JLabel lblId = new JLabel("Id");
		lblId.setBounds(217, 108, 46, 14);
		add(lblId);
	}

	private void registrar() {
		City ciudad = new City();
		ciudad.setDescripcion(tfDescripcion.getText());
		ciudad.setCountryId(Long.parseLong(tfCountryId.getText()));
		ciudad = main.registrarCiudad(ciudad);
		tfId.setText(ciudad.getId().toString());
	}

	KeyListener kl = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				if (e.getSource().equals(tfCountryId)) {
					tfDescripcion.requestFocus();
					registrar();
				}
				if (e.getSource().equals(tfDescripcion)) {
					tfCountryId.requestFocus();
				}
			}
			if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				if (e.getSource().equals(tfDescripcion)) {
					tfCountryId.requestFocus();
				}
			} else if (e.getKeyCode() == KeyEvent.VK_UP) {
				if (e.getSource().equals(tfCountryId)) {
					tfDescripcion.requestFocus();
				}
			}
		}
	};

	ActionListener al = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Integer.parseInt(tfCountryId.getText());
				registrar();
				tfDescripcion.requestFocus();
			} catch (Exception e2) {
				main.lanzarPopUp("el country id solo puede ser numeros");
				tfCountryId.setText("");
			}
		}
	};

	@Override
	public void initialize() {
		tfDescripcion.requestFocus();
	}
}
