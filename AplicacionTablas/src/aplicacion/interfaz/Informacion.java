package aplicacion.interfaz;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

public class Informacion extends JDialog {

	private static final long serialVersionUID = 4914389062420853266L;
	private final JPanel panel = new JPanel();
	private final JButton btAceptar = new JButton("Aceptar");

	public static void main(String[] args) {
		try {
			Informacion dialog = new Informacion();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Informacion() {
		setBounds(100, 100, 570, 140);
		setLocationRelativeTo(getParent());
		setIconImage(Toolkit.getDefaultToolkit().getImage("src/aplicacion/interfaz/imagen/icono_app.png"));
		setTitle("Acerca De");
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		panel.setLayout(new FlowLayout());
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(panel, BorderLayout.CENTER);

		JTextArea contenido = new JTextArea();
		contenido.setOpaque(false);
		contenido.setEditable(false);
		contenido.setText("Esta es una aplicacion que trata sobre busqueda y creacion de ciudades\n"
				+ "\t- para buscar una ciudad debe aceder a [Ciudades --> Buscar]\n"
				+ "\t- para dar de alta una ciudad debe aceder a [Ciudades --> Alta]");
		panel.add(contenido);
		panel.add(btAceptar);
		btAceptar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

}
