package aplicacion.interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import aplicacion.Main;
import aplicacion.modelo.City;
import aplicacion.modelo.TableModel;
import javax.swing.JTextField;

public class ConsultarCiudades extends AbstractView implements MouseListener {

	private static final long serialVersionUID = -8681778875350077255L;
	private TableModel modelo;
	private JTextField tfFiltro;
	private List<City> datos;
	private Main main;
	private JTable tabla_ciudades;

	public ConsultarCiudades(Main main) {
		this.main = main;
		setLayout(null);
		datos = new ArrayList<>();
		addComponents();
	}

	public void addComponents() {
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(57, 79, 584, 302);
		add(scrollPane);

		tabla_ciudades = new JTable();
		scrollPane.setViewportView(tabla_ciudades);

		tfFiltro = new JTextField();
		tfFiltro.setBounds(112, 25, 225, 43);
		tfFiltro.setColumns(10);
		add(tfFiltro);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(435, 24, 122, 44);
		btnConsultar.addActionListener(al);
		add(btnConsultar);

		modelo = new TableModel(datos, nombres());

		tabla_ciudades = new JTable(modelo);
		scrollPane.setViewportView(tabla_ciudades);
		tabla_ciudades.getTableHeader().addMouseListener(this);
	}

	private List<String> nombres() {
		List<String> nombres = new ArrayList<String>();
		nombres.add("id");
		nombres.add("descripcion");
		nombres.add("countryId");
		return nombres;
	}

	public void obtenerDatos() {
		datos = main.getCiudades(tfFiltro.getText());
		if (datos == null) {
			datos = new ArrayList<>();
		}
		modelo.setDatos(datos);
		modelo.fireTableDataChanged();
		tfFiltro.setText("");
	}

	@Override
	public void initialize() {
	}

	ActionListener al = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			obtenerDatos();
			tfFiltro.requestFocus();
		}
	};

	KeyListener kl = new KeyListener() {
		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				obtenerDatos();
				tfFiltro.requestFocus();
			}
		}
	};

	@Override
	public void mouseClicked(MouseEvent e) {
		String nombre = tabla_ciudades.getColumnName(tabla_ciudades.columnAtPoint(e.getPoint()));
		List<String> nombres = nombres();
		modelo.ordenar(nombre, nombres);
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
}
