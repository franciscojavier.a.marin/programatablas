package aplicacion.interfaz;

import javax.swing.JPanel;

public abstract class AbstractView extends JPanel {

	private static final long serialVersionUID = 6636161156327632113L;

	public abstract void initialize();

}
